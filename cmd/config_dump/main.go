package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/tools/imports"

	"github.com/prometheus/blackbox_exporter/config"
)

var (
	configFile  = flag.String("c", "blackbox.yml", "Path to config")
	outFile     = flag.String("o", "", "Path to output file")
	varName     = flag.String("v", "Config", "Config variable name in generated code")
	packageName = flag.String("p", "main", "Package name in generated code")

	sc = &config.SafeConfig{C: &config.Config{}}
)

func main() {
	flag.Parse()
	if *outFile == "" {
		log.Fatal("-o required")
	}
	if err := sc.ReloadConfig(*configFile); err != nil {
		log.Fatal(err)

	}
	f, err := os.Create(*outFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	buf := &bytes.Buffer{}
	fmt.Fprintf(buf, "package %s\n\nvar %s = %#v", *packageName, *varName, sc.C)
	dat, err := imports.Process(*outFile, buf.Bytes(), nil)
	if err != nil {
		log.Fatal("err", err, "file", string(buf.Bytes()))
	}
	ioutil.WriteFile(*outFile, dat, 0x644)
}
